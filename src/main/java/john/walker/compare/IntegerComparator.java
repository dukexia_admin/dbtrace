package john.walker.compare;

import java.util.Comparator;

/**
 * @author Johnnie Walker
 *
 */
public class IntegerComparator implements Comparator<Integer> {

	private static IntegerComparator me = new IntegerComparator();

	@Override
	public int compare(Integer o1, Integer o2) {
		if(o1 == null || o2 == null) {
			return 0;
		} else {
			return o1.intValue() - o2.intValue();
		}
	}

	public static IntegerComparator me() {
		return IntegerComparator.me;
	}
}
